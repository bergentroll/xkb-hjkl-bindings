#hjkl-navigation bindings

##Description

This repo contains file named
**[tweak](https://github.com/py_tosha/xkb-hjkl-bindings/tweak)**.That is a configuration file for
[xkb](https://www.x.org/wiki/XKB/) and it contains examples of binding some
keyboard shortcuts for vim-like and emacs-like naviagtion (also bash uses emacs-style shortcuts). *CapsLock* used as a modifier key.

##Using

To apply this config put **tweak** file to ~/.config/xkb/symbols. Than add to some
startup script (eg ~/.xprofile to apply config when X session stats) line like
that:


```
setxkbmap -layout "tweak(en),tweak(ru)" -option "grp:alt_shift_toggle" -print | xkbcomp -I"$HOME/.config/xkb" - "${DISPLAY%%.*}" -w0
```

In this example xkb loads english and russian layouts through tweak file with its additions, also it sets *Alt+Shift* as combination to switch layouts.
Keep in mind, that setxkbmap execution cancels other xkb session settings.

##Customisation

You can tune your layouts through editing **tweak** file.
By default vim-style navigation is applied. You can also change it to emacs-style by
replacement "include "tweak(vim)" to "tweak(emacs) in general section of the **tweak**
file (see line 4).
You also able to add any other layouts.

##Links

* [Статья на хабре, которая помогла сделать этот конфиг](https://habrahabr.ru/post/222285/)
* [Еще одна вдохновляющая статья](http://iportnov.blogspot.ru/2007_07_01_archive.html)
